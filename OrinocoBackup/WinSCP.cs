﻿using System;
using WinSCP;
using System.IO;

#region WinSCPNotFoundError
/* If you run into problem where WinSCP not found
 * Go to Tools->NuGet_Package_Manager->Manage_NuGet_Packages_for_Solution
 * Click 'Installed' and search for WinSCP
 * Uninstall WinSCP
 * Once uninstalled or if not installed in first place:
 * Click 'Browse' and search 'WinSCP'
 * Install WinSCP 
 */
#endregion

namespace OrinocoBackup
{
    public class WinSCP
    {
        private string  dirName,            
                        username,
                        password,
                        remoteDir,
                        remoteSubDir;

        public WinSCP()
        {
            // Local Directory to store tmp files
            // May need to change depending on machine
            dirName = @"c:\Orinoco Backups.tmp";
            remoteDir = "Orinoco Backup -";
            remoteSubDir = DateTime.Now.ToString("yyyy-MM-dd");
            username = "Team2";            
            password = "";

            Start();
        }

        ~WinSCP()
        {
            // Remove the tmp folder from local machine
            try
            {
                Directory.Delete(dirName, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        /* Connect to WinSCP remote Server
         * @Param string username eg. "Team2" or "Orinoco"
         * @Param string password eg. password used to logon respective server  
         * @Return sessionOptions object
         */
        private SessionOptions Create_Session_Options(string username, string password)
        {          
            // Setup session options
            SessionOptions sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = "home646565961.1and1-data.host",
                UserName = "u86358048-" + username,
                Password = password,
                SshHostKeyFingerprint = "ssh-ed25519 256 0d:15:61:04:10:c2:d7:ce:af:55:68:0a:7c:9c:b8:d7"
            };

            return sessionOptions;
        }


        /* Creates a session using Get_Session_Options return value for verificaion
         * @Param string username eg. "Team2" or "Orinoco"
         * @Param string password eg.password used to logon respective server
         * @Return Session session with an open connection / open session
         */
        private Session Create_Session(string username, string password)
        {

            try
            {
                // Get session options then establish connection with file server
                SessionOptions sessionOptions = Create_Session_Options(username, password);

                // Create session
                Session session = new Session();

                // Open connection / start session
                session.Open(sessionOptions);

                return session;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: {0}", ex);
            }

            // Return nothing if checks fail
            return null;
        }


        // Testing new feature (checking for the age of each directory on the remote server (temam2))
        private void Check_Directory_Age()
        {
            // Create an open session
            Session session = Create_Session(username, password);

            if (session != null)
            {
                SessionOptions options = Create_Session_Options(username, password);

                RemoteDirectoryInfo directory = session.ListDirectory(options.HostName.ToString());
            }
        }


        /* Checks to see if directory exists 
         * Creates the directory if it doesn't already exist
         * @Return bool true if no errors
         * @Return bool false if errors
         */
        private bool Create_Local_Directory()
        {
            // Create a local directory to work out of
            // First check if local directory already exists
            try
            {
                // If dir does not exist, create new dir
                if (!Directory.Exists(dirName))
                {
                    DirectoryInfo dir = Directory.CreateDirectory(dirName);
                    Console.WriteLine("The temp directory was created successfully at {0}.", Directory.GetCreationTime(dirName));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            finally { }

            // Return true if no errors
            return true;
        }
    
        /* Combines the 2 main methods into one
         * Allowing a bit more modularity in future   
         */
        public void Push_Orinoco_To_Admin()
        {
            Pull_From_Orinoco();
            Push_to_Admin();
        }

        /* Pull all files and folders from Orinoco shared root directory
         * onto the local machine
         */
        private void Pull_From_Orinoco()
        {
            // Credentials for Orinoco access
            string username = "Orinoco";           
            string password = "o*(#Tc.u=d"; // TODO hash and store password somwhere else

            // Create local directory if one doesn't exist already
            if (!Create_Local_Directory())
            {
                Console.WriteLine("Upload Failed");
            }
            else
            {
                // Display message
                Console.WriteLine("Downloading...");

                // Create an open session
                Session session = Create_Session(username, password);

                /* If the session exists (has been successfully created)
                 */
                     
                if (session != null)
                {                   
                    // Create and set the transfer options ready for GetFiles()
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    // Instatiate TransferOpResult and assign 
                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles("/*", dirName + "/" + " " + remoteSubDir + "\\" , false, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
                        Console.WriteLine("Download of {0} succeeded", transfer.FileName);
                    }

                    // Dispose of session
                    session.Dispose();
                }
                
            }
        }

        /* Upload all files from local directory to the team2 server
         * @Param string username eg. "Team2" or "Orinoco"
         * @Param string password eg. password used to logon respective server
         */
        private void Push_to_Admin()
        {
            // Start session
            Session session = Create_Session(username, password);

            if (session != null)
            {
                // Display message
                Console.WriteLine("Uploading...");

                // Upload files
                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;

                TransferOperationResult transferResult;
                transferResult = session.PutFiles(dirName + "\\*", "/" + remoteDir + "\\" + remoteSubDir , false, transferOptions);
                
                // Throw on any error
                transferResult.Check();

                // Print results
                foreach (TransferEventArgs transfer in transferResult.Transfers)
                {
                    Console.WriteLine("Upload of {0} succeeded", transfer.FileName);
                }

                // Dispose session
                session.Dispose();

                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
        }

        /* Creates an input mask for password entry. Eg. abc = ***
         * Code sourced from : 
         * http://rajeshbailwal.blogspot.co.uk/2012/03/password-in-c-console-application.html
         */
        private static string Read_Password()
        {
            string password = "";
            ConsoleKeyInfo info = Console.ReadKey(true);
            while (info.Key != ConsoleKey.Enter)
            {
                if (info.Key != ConsoleKey.Backspace)
                {
                    Console.Write("*");
                    password += info.KeyChar;
                }
                else if (info.Key == ConsoleKey.Backspace)
                {
                    if (!string.IsNullOrEmpty(password))
                    {
                        // remove one character from the list of password characters
                        password = password.Substring(0, password.Length - 1);
                        // get the location of the cursor
                        int pos = Console.CursorLeft;
                        // move the cursor to the left by one character
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                        // replace it with space
                        Console.Write(" ");
                        // move the cursor to the left by one character again
                        Console.SetCursorPosition(pos - 1, Console.CursorTop);
                    }
                }
                info = Console.ReadKey(true);
            }

            // Add a new line after user presses enter
            Console.WriteLine();

            return password;
        }

        // Start program loop
        private void Start()
        {
            Console.Title = "Krautsoft Server Backup Utility";
            string choice = "0";

            while (choice != "n")
            {
                Console.Clear();
                Console.WriteLine("Admin Login");
                Console.Write("Password-> ");
                password = Read_Password();

                Session session = Create_Session(username, password);

                if (session != null)
                {
                    Display_Options(); // Display all options
                    choice = "n"; // Finishes program
                }
                else
                {
                    Console.WriteLine("Would you like to try again? [ y / n ]");
                    choice = Console.ReadLine();
                }
            }
        }

        // Display Options Menu
        private void Display_Options()
        {
            string choice = "0";
            Console.Clear();

            while (choice != "3")
            {
                choice = "0"; // Reset choice to default

                Console.Clear();
                Console.WriteLine("Select a task:\n");
                Console.WriteLine("[ 1 ] Backup Orinoco project directories.");
                //Console.WriteLine("[ 2 ] Restore Orinoco project directories.");
                Console.WriteLine("[ 2 ] Refresh Backups.");
                Console.WriteLine("[ 3 ] Quit.");
                choice = Console.ReadLine();

                switch (choice)
                {
                    case "1":
                        Console.Clear();
                        Push_Orinoco_To_Admin();
                        break;
                    case "2":
                        break;
                    case "3":
                        break;
                    default:
                        Console.WriteLine("Invalid selection. Try again.");
                        Console.WriteLine("Press any key to continue...");
                        choice = Console.ReadLine();
                        Console.Clear();
                        break;
                }
            }
        }
    }
}